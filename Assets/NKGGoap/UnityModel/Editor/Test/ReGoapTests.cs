﻿using System.Collections.Generic;
using ReGoap.Core;
using ReGoap.Planner;
using ReGoap.Unity.Test;
using NUnit.Framework;
using ReGoap.Utilities;
using UnityEngine;

namespace ReGoap.Unity.Editor.Test
{
    /// <summary>
    /// 压力测试类
    /// </summary>
    public class ReGoapTests
    {
        [OneTimeSetUp]
        public void Init()
        {
        }

        [OneTimeTearDown]
        public void Dispose()
        {
        }

        /// <summary>
        /// 获取Planner（新建一个）
        /// </summary>
        /// <param name="dynamicActions"></param>
        /// <returns></returns>
        IGoapPlanner<string, object> GetPlanner(bool dynamicActions = false)
        {
            // not using early exit to have precise results, probably wouldn't care in a game for performance reasons
            return new ReGoapPlanner<string, object>(
                new ReGoapPlannerSettings
                    {PlanningEarlyExit = false, UsingDynamicActions = dynamicActions, DebugPlan = true}
            );
        }

        /// <summary>
        /// 准备AI代理
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        ReGoapTestAgent PrepareAgent(GameObject owner)
        {
            var agent = owner.AddComponent<ReGoapTestAgent>();
            agent.Init();
            return agent;
        }

        /// <summary>
        /// 测试简单Plan
        /// </summary>
        [Test]
        public void TestSimpleChainedPlan()
        {
            TestSimpleChainedPlan(GetPlanner());
        }

        /// <summary>
        /// 测试两个可能的Plan
        /// </summary>
        [Test]
        public void TestTwoPhaseChainedPlan()
        {
            TestTwoPhaseChainedPlan(GetPlanner());
        }

        /// <summary>
        /// 测试State不同对比
        /// </summary>
        [Test]
        public void TestReGoapStateMissingDifference()
        {
            var state = ReGoapState<string, object>.Instantiate();
            state.Set("var0", true);
            state.Set("var1", "string");
            state.Set("var2", 1);
            var otherState = ReGoapState<string, object>.Instantiate();
            otherState.Set("var1", "stringDifferent");
            otherState.Set("var2", 1);
            var differences = ReGoapState<string, object>.Instantiate();
            var count = state.MissingDifference(otherState, ref differences);
            Assert.That(count, Is.EqualTo(2));
            Assert.That(differences.Get("var0"), Is.EqualTo(true));
            Assert.That(differences.Get("var1"), Is.EqualTo("string"));
            Assert.That(differences.HasKey("var2"), Is.EqualTo(false));
        }

        /// <summary>
        /// 测试State加法运算符
        /// </summary>
        [Test]
        public void TestReGoapStateAddOperator()
        {
            var state = ReGoapState<string, object>.Instantiate();
            state.Set("var0", true);
            state.Set("var1", "string");
            state.Set("var2", 1);
            var otherState = ReGoapState<string, object>.Instantiate();
            otherState.Set("var2", "new2"); // 2nd one replaces the first
            otherState.Set("var3", true);
            otherState.Set("var4", 10.1f);
            Assert.That(state.Count, Is.EqualTo(3));
            state.AddFromState(otherState);
            Assert.That(otherState.Count, Is.EqualTo(3));
            Assert.That(state.Count, Is.EqualTo(5)); // var2 on first is replaced by var2 on second
            Assert.That(state.Get("var0"), Is.EqualTo(true));
            Assert.That(state.Get("var1"), Is.EqualTo("string"));
            Assert.That(state.Get("var2"), Is.EqualTo("new2"));
            Assert.That(state.Get("var3"), Is.EqualTo(true));
            Assert.That(state.Get("var4"), Is.EqualTo(10.1f));
        }

        /// <summary>
        /// 测试冲突Action的Plan
        /// </summary>
        [Test]
        public void TestConflictingActionPlan()
        {
            var gameObject = new GameObject();

            //Action：{名称：跳入水中}，{先决条件：Pos = 0}，{效果：Pos = 1，isSwimming = true}，{权重：1}
            ReGoapTestsHelper.GetCustomAction(gameObject, "JumpIntoWater",
                new Dictionary<string, object> {{"isAtPosition", 0}},
                new Dictionary<string, object> {{"isAtPosition", 1}, {"isSwimming", true}}, 1);

            //Action：{名称：去游泳}，{先决条件：无}，{效果：Pos = 0}，{权重：1}
            ReGoapTestsHelper.GetCustomAction(gameObject, "GoSwimming",
                new Dictionary<string, object> { },
                new Dictionary<string, object> {{"isAtPosition", 0}}, 2);

            //Goal：{名称：游泳}，{目标状态：isSwimming = true}，{优先级：1}
            var hasAxeGoal = ReGoapTestsHelper.GetCustomGoal(gameObject, "Swim",
                new Dictionary<string, object> {{"isSwimming", true}});

            var memory = gameObject.AddComponent<ReGoapTestMemory>();
            memory.Init();

            //进行AI代理，Action，Memory，Goal初始化
            var agent = PrepareAgent(gameObject);

            //进行路径规划
            var plan = GetPlanner().Plan(agent, null, null, null);

            
            //断言规划好的路径结果为hasAxeGoal
            Assert.That(plan, Is.EqualTo(hasAxeGoal));
            // validate plan actions
            //正式使Plan生效
            ReGoapTestsHelper.ApplyAndValidatePlan(plan, agent, memory);
        }

        /// <summary>
        /// 链式Plan
        /// </summary>
        /// <param name="planner"></param>
        public void TestSimpleChainedPlan(IGoapPlanner<string, object> planner)
        {
            var gameObject = new GameObject();

            //Debug.Log("第一次----------------------");

            //Action：{名称：建造Axe}，{先决条件：hasWood = true，hasSteel = true}，{效果：hasAxe = true，hasWood = false，hasSteel = false}，{权重：10}
            ReGoapTestsHelper.GetCustomAction(gameObject, "CreateAxe",
                new Dictionary<string, object> {{"hasWood", true}, {"hasSteel", true}},
                new Dictionary<string, object> {{"hasAxe", true}, {"hasWood", false}, {"hasSteel", false}}, 10);

            //Action：{名称：砍树}，{先决条件：无}，{效果：hasRawWood = true}，{权重：2}
            ReGoapTestsHelper.GetCustomAction(gameObject, "ChopTree",
                new Dictionary<string, object> { },
                new Dictionary<string, object> {{"hasRawWood", true}}, 2);

            //Action：{名称：加工原木}，{先决条件：hasRawWood = true}，{效果：hasRawWood = false，hasWood = true}，{权重：5}
            ReGoapTestsHelper.GetCustomAction(gameObject, "WorksWood",
                new Dictionary<string, object> {{"hasRawWood", true}},
                new Dictionary<string, object> {{"hasWood", true}, {"hasRawWood", false}}, 5);

            //Action：{名称：拥有矿山}，{先决条件：无}，{效果：hasOre = true}，{权重：10}
            ReGoapTestsHelper.GetCustomAction(gameObject, "MineOre",
                new Dictionary<string, object> { },
                new Dictionary<string, object> {{"hasOre", true}}, 10);

            //Action：{名称：加工矿石}，{先决条件：hasOre = true}，{效果：hasOre = false，hasSteel = ture}，{权重：10}
            ReGoapTestsHelper.GetCustomAction(gameObject, "SmeltOre",
                new Dictionary<string, object> {{"hasOre", true}},
                new Dictionary<string, object> {{"hasSteel", true}, {"hasOre", false}}, 10);

            //Goal：{名称：拥有Axe}，{目标状态：hasAxe = true}，{优先级：1}
            var hasAxeGoal = ReGoapTestsHelper.GetCustomGoal(gameObject, "HasAxeGoal",
                new Dictionary<string, object> {{"hasAxe", true}}, 1);

            //Goal：{名称：拥有Axe和矿山}，{目标状态：hasAxe = true，hasOre = true，isGreedy = true}，{优先级：2}
            var greedyHasAxeAndOreGoal = ReGoapTestsHelper.GetCustomGoal(gameObject, "GreedyHasAxeAndOreGoal",
                new Dictionary<string, object> {{"hasAxe", true}, {"hasOre", true}, {"isGreedy", true}},
                2);

            var memory = gameObject.AddComponent<ReGoapTestMemory>();
            memory.Init();

            var agent = PrepareAgent(gameObject);

            var plan = planner.Plan(agent, null, null, null);

            Assert.That(plan, Is.EqualTo(hasAxeGoal));
            // validate plan actions
            ReGoapTestsHelper.ApplyAndValidatePlan(plan, agent, memory);
            
            // now we set the agent to be greedy, so the second goal can be activated
            memory.SetValue("isGreedy", true);
            // now the planning should choose KillEnemyGoal
            plan = planner.Plan(agent, null, null, null);
            
            Assert.That(plan, Is.EqualTo(greedyHasAxeAndOreGoal));
            ReGoapTestsHelper.ApplyAndValidatePlan(plan, agent, memory);
        }

        /// <summary>
        /// 测试两个可能的链式Plan
        /// </summary>
        /// <param name="planner"></param>
        public void TestTwoPhaseChainedPlan(IGoapPlanner<string, object> planner)
        {
            var gameObject = new GameObject();
            //Action：{名称：开枪}，{先决条件：hasWeaponEquipped = true，isNearEnemy = true}，{效果：killedEnemy = true}，{权重：4}
            ReGoapTestsHelper.GetCustomAction(gameObject, "CCAction",
                new Dictionary<string, object> {{"hasWeaponEquipped", true}, {"isNearEnemy", true}},
                new Dictionary<string, object> {{"killedEnemy", true}}, 4);

            //Action：{名称：装备武器}，{先决条件：hasAxe = true}，{效果：hasWeaponEquipped = true}，{权重：1}
            ReGoapTestsHelper.GetCustomAction(gameObject, "EquipAxe",
                new Dictionary<string, object> {{"hasAxe", true}},
                new Dictionary<string, object> {{"hasWeaponEquipped", true}}, 1);

            //Action：{名称：朝敌人走去}，{先决条件：hasTarget = true}，{效果：isNearEnemy = true}，{权重：3}
            ReGoapTestsHelper.GetCustomAction(gameObject, "GoToEnemy",
                new Dictionary<string, object> {{"hasTarget", true}},
                new Dictionary<string, object> {{"isNearEnemy", true}}, 3);

            //Action：{名称：创建Axe}，{先决条件：hasWood = true，hasSteel = true}，{效果：hasAxe = true，hasWood = false，hasSteel = false}，{权重：10}
            ReGoapTestsHelper.GetCustomAction(gameObject, "CreateAxe",
                new Dictionary<string, object> {{"hasWood", true}, {"hasSteel", true}},
                new Dictionary<string, object> {{"hasAxe", true}, {"hasWood", false}, {"hasSteel", false}}, 10);

            //Action：{名称：砍树}，{先决条件：无}，{效果：hasRawWood = true}，{权重：2}
            ReGoapTestsHelper.GetCustomAction(gameObject, "ChopTree",
                new Dictionary<string, object> { },
                new Dictionary<string, object> {{"hasRawWood", true}}, 2);

            //Action：{名称：加工原木}，{先决条件：hasRawWood = true}，{效果：hasWood = true，hasRawWood = false}，{权重：5}
            ReGoapTestsHelper.GetCustomAction(gameObject, "WorksWood",
                new Dictionary<string, object> {{"hasRawWood", true}},
                new Dictionary<string, object> {{"hasWood", true}, {"hasRawWood", false}}, 5);

            //Action：{名称：拥有矿石}，{先决条件：无}，{效果：hasOre = true}，{权重：10}
            ReGoapTestsHelper.GetCustomAction(gameObject, "MineOre", new Dictionary<string, object> { },
                new Dictionary<string, object> {{"hasOre", true}}, 10);

            //Action：{名称：加工矿石}，{先决条件：hasOre = true}，{效果：hasOre = false，hasSteel = true}，{权重：10}
            ReGoapTestsHelper.GetCustomAction(gameObject, "SmeltOre",
                new Dictionary<string, object> {{"hasOre", true}},
                new Dictionary<string, object> {{"hasSteel", true}, {"hasOre", false}}, 10);

            //Goal：{名称：准备好去攻击目标}，{目标状态：hasWeaponEquipped = true}，{优先级：2}
            var readyToFightGoal = ReGoapTestsHelper.GetCustomGoal(gameObject, "ReadyToFightGoal",
                new Dictionary<string, object> {{"hasWeaponEquipped", true}}, 2);

            //Goal：{名称：有Axe目标}，{目标状态：hasAxe = true}，{优先级：1}
            ReGoapTestsHelper.GetCustomGoal(gameObject, "HasAxeGoal",
                new Dictionary<string, object> {{"hasAxe", true}});

            //Goal：{名称：击杀目标}，{目标状态：killedEnemy = true}，{优先级：3}
            var killEnemyGoal = ReGoapTestsHelper.GetCustomGoal(gameObject, "KillEnemyGoal",
                new Dictionary<string, object> {{"killedEnemy", true}}, 3);

            var memory = gameObject.AddComponent<ReGoapTestMemory>();
            memory.Init();

            var agent = PrepareAgent(gameObject);


            // first plan should create axe and equip it, through 'ReadyToFightGoal', since 'hasTarget' is false (memory should handle this)
            //第一个Plan应该创建Axe并装备他（通过“准备就绪去攻击目标”Goal）因为此时hasTarget为Flase
            var plan = planner.Plan(agent, null, null, null);

            Assert.That(plan, Is.EqualTo(readyToFightGoal));
            // we apply manually the effects, but in reality the actions should do this themselves 
            //  and the memory should understand what happened 
            //  (e.g. equip weapon action? memory should set 'hasWeaponEquipped' to true if the action equipped something)
            // validate plan actions
            // 我们手动应用效果，但实际上，动作应自行完成，记忆应该了解发生了什么
            //（例如，装备武器动作，如果该动作装备了某些东西，记忆应将“ hasWeaponEquipped”设置为true）
            // 验证计划行动
            ReGoapTestsHelper.ApplyAndValidatePlan(plan, agent, memory);

            // now we tell the memory that we see the enemy
            //现在告诉Memory我们发现了敌人
            memory.SetValue("hasTarget", true);
            // now the planning should choose KillEnemyGoal
            // 现在Plan应该会选择“杀死敌人”这一目标
            plan = planner.Plan(agent, null, null, null);

            Assert.That(plan, Is.EqualTo(killEnemyGoal));
            ReGoapTestsHelper.ApplyAndValidatePlan(plan, agent, memory);
        }

        // Additional test by TPMxyz
        /// <summary>
        /// TPMxyz贡献的测试用例
        /// 收集到收集
        /// </summary>
        [Test]
        public void TestGatherGotoGather()
        {
            var gameObject = new GameObject();
            //Action：{名称：收集苹果}，{先决条件：At = Farm}，{效果：hasApple = true}，{权重：1}
            ReGoapTestsHelper.GetCustomAction(gameObject, "GatherApple",
                new Dictionary<string, object> {{"At", "Farm"}},
                new Dictionary<string, object> {{"hasApple", true}}, 1);

            //Action：{名称：收集桃子}，{先决条件：At = Farm}，{效果：hasPeach = true}，{权重：2}
            ReGoapTestsHelper.GetCustomAction(gameObject, "GatherPeach",
                new Dictionary<string, object> {{"At", "Farm"}},
                new Dictionary<string, object> {{"hasPeach", true}}, 2);

            //Action：{名称：去}，{先决条件：无}，{效果：At = Farm}，{权重：2}
            ReGoapTestsHelper.GetCustomAction(gameObject, "Goto",
                new Dictionary<string, object> { },
                new Dictionary<string, object> {{"At", "Farm"}}, 10);

            //Goal：{名称：收集所有}，{目标状态：hasApple = true，hasPeach = true}，{优先级：2}
            var theGoal = ReGoapTestsHelper.GetCustomGoal(gameObject, "GatherAll",
                new Dictionary<string, object> {{"hasApple", true}, {"hasPeach", true}});

            var memory = gameObject.AddComponent<ReGoapTestMemory>();
            memory.Init();

            var agent = gameObject.AddComponent<ReGoapTestAgent>();
            agent.Init();

            var plan = GetPlanner().Plan(agent, null, null, null);

            Assert.That(plan, Is.EqualTo(theGoal));
            // validate plan actions
            ReGoapTestsHelper.ApplyAndValidatePlan(plan, agent, memory);
        }

        // Additional test by TPMxyz
        /// <summary>
        /// TPMxyz贡献的测试用例
        /// 测试Action覆盖Goal
        /// </summary>
        [Test]
        public void TestActionOverrideGoal()
        {
            var gameObject = new GameObject();

            //Action：{名称：拥有矿山}，{先决条件：无}，{效果：hasMoney = true}，{权重：10}
            ReGoapTestsHelper.GetCustomAction(gameObject, "Mine Ore",
                new Dictionary<string, object> { },
                new Dictionary<string, object> {{"hasMoney", true}}, 10);

            //Action：{名称：购买食物}，{先决条件：hasMoney = true}，{效果：hasFood = true，hasMoney = false}，{权重：2}
            ReGoapTestsHelper.GetCustomAction(gameObject, "Buy Food",
                new Dictionary<string, object> {{"hasMoney", true}},
                new Dictionary<string, object> {{"hasFood", true}, {"hasMoney", false}}, 2);

            //Goal：{名称：准备钱和食物}，{目标状态：hasMoney = true，hasFood = true}，{优先级：2}
            var theGoal = ReGoapTestsHelper.GetCustomGoal(gameObject, "PrepareFoodAndMoney",
                new Dictionary<string, object> {{"hasMoney", true}, {"hasFood", true}});

            var memory = gameObject.AddComponent<ReGoapTestMemory>();
            memory.Init();

            var agent = gameObject.AddComponent<ReGoapTestAgent>();
            agent.Init();

            var plan = GetPlanner().Plan(agent, null, null, null);

            Assert.That(plan, Is.EqualTo(theGoal));
            // validate plan actions
            ReGoapTestsHelper.ApplyAndValidatePlan(plan, agent, memory);
        }

        [Test]
        public void TestDynamicAction()
        {
            var gameObject = new GameObject();

            // settings these value to make sure that the planning chooses weaponA, since the pathing
            // weaponA -> ammoA -> enemy is actually cheaper, even if weaponC is very close to the enemy
            // without dynamic cost weaponC would always be chosen
            // we also add weaponB and ammoB to show the reconcileStartPosition logic
            // with this requirement in the goal and this action, we push as cost a goto to the player position
            // this effectively makes the plan: weaponB -> ammoB -> enemy more expensive, without reconciling with the starting position this plan would have been the best.
            // 设置这些值，以确保计划从路径开始就选择武器A
            // 即使武器C非常靠近敌人，但武器A->弹药->敌人总的来说消耗更小，
            // 没有动态权重的话，武器C将始终被选择
            // 我们还添加了武器B和弹药B以展示reconcileStartPosition逻辑
            // 根据Goal和Action的要求，我们新增动态Goto权重
            // 这有效地使该计划：武器B->弹药->敌人更加昂贵，但是如果不考虑和起始点进行协调，那么这个方案就是最好的。
            var playerPosition = Vector2.zero;
            var enemyPosition = new Vector2(0, 100);
            var weaponAPosition = new Vector2(0, 50);
            var ammoAPosition = new Vector2(0, 60);
            var weaponBPosition = new Vector2(0, 115);
            var ammoBPosition = new Vector2(0, 115);
            var weaponCPosition = new Vector2(-5, 100);
            var weaponRange = 20.0f;

            //Action：{名称：射击敌人}，{先决条件：weaponReady = true，isAt = enemyPosition，inRange = weaponRange}，{效果：shootEnemy = true}，{权重：100}
            ReGoapTestsHelper.GetCustomAction(gameObject, "ShootEnemy",
                new Dictionary<string, object>
                {
                    {"weaponReady", true}, {"isAt", enemyPosition}, {"inRange", weaponRange}
                },
                new Dictionary<string, object> {{"shootEnemy", true}}, 100);

            //Action：{名称：重新装弹}，{先决条件：hasWeapon = true，hasAmmo = true}，{效果：weaponReady = true}，{权重：20}
            ReGoapTestsHelper.GetCustomAction(gameObject, "ReloadWeapon",
                new Dictionary<string, object> {{"hasWeapon", true}, {"hasAmmo", true}},
                new Dictionary<string, object> {{"weaponReady", true}}, 20);

            #region getWeapon 获取武器

            //Action：{名称：获取武器}，{先决条件：无}，{效果：hasWeapon = true}，{权重：5}
            var getWeapon = ReGoapTestsHelper.GetCustomAction(gameObject, "GetWeapon",
                new Dictionary<string, object> { },
                new Dictionary<string, object> {{"hasWeapon", true}}, 5);

            //自定义先决条件获得者，会根据stackData更改isAt所对应的值
            getWeapon.CustomPreconditionsGetter = (ref ReGoapState<string, object> preconditions,
                GoapActionStackData<string, object> stackData) =>
            {
                preconditions.Clear();
                if (stackData.settings.HasKey("weaponPosition"))
                {
                    preconditions.Set("isAt", (Vector2) stackData.settings.Get("weaponPosition"));
                }
            };

            //自定义效果获得者，会根据stackData更改hasWeapon所对应的值
            getWeapon.CustomEffectsGetter = (ref ReGoapState<string, object> effects,
                GoapActionStackData<string, object> stackData) =>
            {
                effects.Clear();
                if (stackData.settings.HasKey("weaponPosition"))
                {
                    effects.Set("hasWeapon", true);
                }
            };

            //自定义设置获得者，会根据stackData更改weaponPosition所对应的值
            getWeapon.CustomSettingsGetter = (GoapActionStackData<string, object> stackData) =>
            {
                var results = new List<ReGoapState<string, object>>();

                if (stackData.currentState.HasKey("weaponPositions") && stackData.currentState.HasKey("isAt"))
                {
                    var currentPosition = (Vector2) stackData.currentState.Get("isAt");

                    foreach (var objectPosition in (List<Vector2>) stackData.currentState.Get("weaponPositions"))
                    {
                        ReGoapState<string, object> settings = ReGoapState<string, object>.Instantiate();
                        settings.Set("weaponPosition", objectPosition);
                        results.Add(settings);
                    }
                }

                return results;
            };

            #endregion

            #region getAmmo 获取弹药

            //Action：{名称：获取Ammo}，{先决条件：无}，{效果：hasAmmo = true}，{权重：3}
            var getAmmo = ReGoapTestsHelper.GetCustomAction(gameObject, "GetAmmo",
                new Dictionary<string, object> { },
                new Dictionary<string, object> {{"hasAmmo", true}}, 3);

            //自定义先决条件获得者，会根据stackData更改isAt所对应的值
            getAmmo.CustomPreconditionsGetter = (ref ReGoapState<string, object> preconditions,
                GoapActionStackData<string, object> stackData) =>
            {
                preconditions.Clear();
                if (stackData.settings.HasKey("ammoPosition"))
                {
                    preconditions.Set("isAt", (Vector2) stackData.settings.Get("ammoPosition"));
                }
            };

            //自定义先决Effect获得者，会根据stackData更改hasAmmo所对应的值
            getAmmo.CustomEffectsGetter = (ref ReGoapState<string, object> effects,
                GoapActionStackData<string, object> stackData) =>
            {
                effects.Clear();
                if (stackData.settings.HasKey("ammoPosition"))
                {
                    effects.Set("hasAmmo", true);
                }
            };

            //自定义设置获得者，会根据stackData更改ammoPosition所对应的值
            getAmmo.CustomSettingsGetter = (GoapActionStackData<string, object> stackData) =>
            {
                var results = new List<ReGoapState<string, object>>();

                if (stackData.currentState.HasKey("ammoPositions") && stackData.currentState.HasKey("isAt"))
                {
                    var currentPosition = (Vector2) stackData.currentState.Get("isAt");

                    foreach (var objectPosition in (List<Vector2>) stackData.currentState.Get("ammoPositions"))
                    {
                        ReGoapState<string, object> settings = ReGoapState<string, object>.Instantiate();
                        settings.Set("ammoPosition", objectPosition);
                        results.Add(settings);
                    }
                }

                return results;
            };

            #endregion

            #region dynamicGoTo 动态去往Action

            //Action：{名称：去往}，{先决条件：}，{效果：无}，{权重：1}
            var dynamicGoTo = ReGoapTestsHelper.GetCustomAction(gameObject, "GoTo",
                new Dictionary<string, object> { },
                new Dictionary<string, object> { });

            //自定义权重获得者，会根据stackData更改权重所对应的值
            dynamicGoTo.CustomCostGetter = (ref float cost, GoapActionStackData<string, object> stackData) =>
            {
                // base value to avoid free action
                cost = 1.0f;
                var inRange = 0.0f;
                if (stackData.settings.HasKey("inRange"))
                {
                    inRange = (float) stackData.settings.Get("inRange");
                }

                if (stackData.settings.HasKey("isAt") && stackData.currentState.HasKey("isAt"))
                {
                    var wantedPosition = (Vector2) stackData.settings.Get("isAt");
                    var currentPosition = (Vector2) stackData.currentState.Get("isAt");
                    cost = (wantedPosition - currentPosition).magnitude - inRange;
                    if (cost < 0) cost = 0;
                }
            };

            //自定义效果获得者，会根据stackData更改isAt，inRange所对应的值
            dynamicGoTo.CustomEffectsGetter = (ref ReGoapState<string, object> effects,
                GoapActionStackData<string, object> stackData) =>
            {
                effects.Clear();
                if (stackData.settings.HasKey("isAt"))
                {
                    var wantedPosition = (Vector2) stackData.settings.Get("isAt");
                    effects.Set("isAt", wantedPosition);
                }

                if (stackData.settings.HasKey("inRange"))
                {
                    var inRange = (float) stackData.settings.Get("inRange");
                    effects.Set("inRange", inRange);
                }
            };

            //自定义设置获得者，会根据stackData更改isAt，inRange所对应的值
            dynamicGoTo.CustomSettingsGetter = (GoapActionStackData<string, object> stackData) =>
            {
                var newSettings = ReGoapState<string, object>.Instantiate();

                Vector2 wantedPosition = Vector2.zero;
                float inRange = 0.0f;
                if (stackData.goalState.HasKey("isAt"))
                {
                    wantedPosition = (Vector2) stackData.goalState.Get("isAt");
                }

                if (stackData.goalState.HasKey("inRange"))
                {
                    inRange = (float) stackData.goalState.Get("inRange");
                }

                newSettings.Set("isAt", wantedPosition);
                newSettings.Set("inRange", inRange);
                return new List<ReGoapState<string, object>> {newSettings};
            };

            #endregion

            #region reconcileStartPosition 调和起始点

            //Action：{名称：调和起始点}，{先决条件：}，{效果：无}，{权重：1}
            var reconcileStartPosition = ReGoapTestsHelper.GetCustomAction(gameObject, "ReconcileStartPosition",
                new Dictionary<string, object> { },
                new Dictionary<string, object> { }, 1);

            //自定义先决条件获得者，会根据stackData更改isAt所对应的值
            reconcileStartPosition.CustomPreconditionsGetter = (ref ReGoapState<string, object> preconditions,
                GoapActionStackData<string, object> stackData) =>
            {
                preconditions.Clear();
                // this could be fetched from the world memory, in a custom action class
                preconditions.Set("isAt", playerPosition);
            };

            //自定义效果获得者，会根据stackData更改reconcileStartPosition所对应的值
            reconcileStartPosition.CustomEffectsGetter = (ref ReGoapState<string, object> effects,
                GoapActionStackData<string, object> stackData) =>
            {
                effects.Clear();
                // we want this action to work only if no other goal has to be archived
                if (stackData.goalState.HasKey("reconcileStartPosition") && stackData.goalState.Count == 1)
                {
                    effects.Set("reconcileStartPosition", true);
                }
            };

            #endregion

            //Goal：{名称：射击敌人}，{目标状态：shootEnemy = true，reconcileStartPosition = true}，{优先级：1}
            var theGoal = ReGoapTestsHelper.GetCustomGoal(gameObject, "ShootEnemy",
                new Dictionary<string, object> {{"shootEnemy", true}, {"reconcileStartPosition", true}});

            var memory = gameObject.AddComponent<ReGoapTestMemory>();
            memory.Init();
            memory.SetValue("enemyPosition", enemyPosition);
            memory.SetValue("ammoPositions", new List<Vector2> {ammoAPosition, ammoBPosition});
            memory.SetValue("weaponPositions", new List<Vector2> {weaponAPosition, weaponBPosition, weaponCPosition});

            var agent = gameObject.AddComponent<ReGoapTestAgent>();
            agent.Init();

            var plan = GetPlanner(dynamicActions: true).Plan(agent, null, null, null);
        }
    }
}