# NKGGOAP

#### 介绍
目标导向的AI系统，致力于解决复杂情况下的FSM与行为树维护问题。

参考ReGoap开源库：[https://github.com/luxkun/ReGoap](https://github.com/luxkun/ReGoap)

ReGoap的中文文档：[https://www.lfzxb.top/goal-oriented-action-planning-chinese-document/](https://www.lfzxb.top/goal-oriented-action-planning-chinese-document/)

FEAR在GDC分享的基于GOAP的AI系统（中英双语）：[https://www.lfzxb.top/gdc-sharing-of-ai-system-based-on-goap-in-fear-simple-cn/](https://www.lfzxb.top/gdc-sharing-of-ai-system-based-on-goap-in-fear-simple-cn/)

GOAP相关资源：[http://alumni.media.mit.edu/~jorkin/goap.html](http://alumni.media.mit.edu/~jorkin/goap.html)

#### 更新计划
目前是通读ReGoap源码，并写上注释，然后视情况进行重构优化，新增JobSystem支持，并附赠一个小Demo。

